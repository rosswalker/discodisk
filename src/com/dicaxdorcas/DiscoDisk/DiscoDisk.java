package com.dicaxdorcas.DiscoDisk;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.plugin.java.JavaPlugin;

public final class DiscoDisk extends JavaPlugin implements Listener {
  
  @Override
  public void onEnable() {
    getServer().getPluginManager().registerEvents(this, this);
  }
  
  @SuppressWarnings("deprecation")
  @EventHandler
  public void onInteract(PlayerInteractEntityEvent event) {
    Entity rightclicked = event.getRightClicked();
    Location loc = rightclicked.getLocation();
    
    // If a player
    if (rightclicked.getType() == EntityType.PLAYER) {
      // If clicking with a record
      if (event.getPlayer().getItemInHand().getTypeId() == 2257) {
        float yaw = rightclicked.getLocation().getYaw() / 90;
        yaw = (float)Math.round(yaw);
     
        loc.setPitch(90F);
        if (yaw == -4 || yaw == 0 || yaw == 4) { // Facing South 
          loc.setYaw(180F);
        }
        if (yaw == -1 || yaw == 3) { // Facing East
          loc.setYaw(90F);
        }
        if (yaw == -2 || yaw == 2) { // Facing North
          loc.setYaw(360F);
        }
        if (yaw == -3 || yaw == 1) { // Facing West 
          loc.setYaw(270F);
        }
        rightclicked.teleport(loc);
      }
    }
    
  }

}


